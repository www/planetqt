/****************************************************************************
**
** Copyright (C) 2015 Digia Plc and/or its subsidiary(-ies).
**
****************************************************************************/

"use strict";

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
    $('.cookies_yum').click(function() {
        $(this).fadeOut()
    });
}
function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}
function eraseCookie(name) {
    createCookie(name, "", -1);
}
function load_sdk(s, id, src) {
    var js, fjs = document.getElementsByTagName(s)[0];
    if (document.getElementById(id)) return;
    js = document.createElement(s);
    js.id = id;
    js.src = src;
    fjs.parentNode.insertBefore(js, fjs);
}
$(document).ready(function($) {
    if (document.documentElement.clientWidth < 1220) {
        oneQt.extraLinksToMain();
    }

    $('#menuextras .search').click(function(e){
        e.preventDefault();
        $('.big_bar.account').slideUp();
        $('.big_bar.search').slideToggle();
        $('.big_bar_search').focus();
        $(this).toggleClass('open');
    });
    $('.cookies_yum').click(function() {
        $('.cookies_yum').fadeOut();
        createCookie("cookies_nom", "yum", 180);
        var cookie_added = 1;
    });
    if (!(readCookie('cookies_nom') == 'yum')) {
        $('.cookies_yum').fadeIn();
    } else {
        var cookie_added = 1;
    }

    $('#navbar .navbar-toggle').click(function(e) {
        e.preventDefault();
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            $('#navbar .navbar-menu').css('max-height', '0px');
        }
        else {
            $(this).addClass('opened');
            $('#navbar .navbar-menu').css('max-height', $('#navbar .navbar-menu ul').outerHeight() + 'px');
        }
    });

    $(window).resize(function() {
        oneQt.stickySidebar();
        oneQt.footerPosition();
        if (document.documentElement.clientWidth < 1220) {
            oneQt.extraLinksToMain();
        } else {
            oneQt.mainLinkstoExtra();
        }
    });

    $(window).scroll(function() {
        oneQt.stickySidebar();
        oneQt.stickyHeader();
    });

    oneQt.stickySidebar();
    oneQt.footerPosition();
});

$( window ).load(function() {
    load_sdk('script', 'facebook-jssdk','//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=207346529386114&version=v2.0');
    load_sdk('script', 'twitter-wjs', '//platform.twitter.com/widgets.js');
});

var oneQt = {
    stickySidebar: function() {
        if ($('#sidebar').length && $('#sidebar').outerHeight() > 20) {
            var $sidebar = $('#sidebar');
            var $win = $(window);
            var $sidebarContainer = $sidebar.parent();
            var headerHeight = $('#navbar').outerHeight();
            if ($win.outerHeight() - headerHeight > $sidebar.innerHeight() &&
                    $win.scrollTop() > $sidebarContainer.offset().top) {
                var newTop = headerHeight + $win.scrollTop() - $sidebarContainer.offset().top;
                if (newTop + $sidebar.innerHeight() > $sidebarContainer.innerHeight())
                    newTop = $sidebarContainer.innerHeight() - $sidebar.innerHeight();

                $sidebar.css({top: newTop +'px'})
            }
            else {
                $sidebar.css({top: '0'})
            }
        }
    },

    footerPosition: function () {
        $('#footerbar').removeClass('fixed');
        if (($('.hbspt-form').length > 0) || ($('#customerInfo').length > 0) || ($('.purchase_bar').length > 0)) {
            var footerBottomPos = $('#footerbar').offset().top + $('#footerbar').outerHeight();
            if (footerBottomPos < $(window).height())
                $('#footerbar').addClass('fixed');
        }
    },

    stickyHeader: function () {
        var originalHeaderHeight = 79;
        if ($(window).scrollTop() > originalHeaderHeight) {
            $('#navbar').addClass('fixed');
            $('#bottom_header').fadeOut();

            if (!(cookie_added == 1)) {
                $('.cookies_yum').fadeOut();
                createCookie("cookies_nom", "yum", 180);
                var cookie_added = 1;
            }
        }
        else {
            $('#navbar').removeClass('fixed');
            $('#bottom_header').fadeIn();
        }
    },
    extraLinksToMain: function() {
        var extramenuLinks = $('#menuextras').find('li');
        var mainmenu = $('#mainmenu');
        var count = 0;
        if ($(extramenuLinks).length > 3) {
            $(extramenuLinks).each(function() {
                if (count < 3) {
                    var newLink = $(this);
                    $(newLink).addClass('dynamic-add');
                    $(mainmenu).append(newLink);
                }
                count++;
            });
        }
    },

    mainLinkstoExtra: function() {
        var mainmenuLinks = $('#mainmenu').find('.dynamic-add');
        var extramenu = $('#menuextras');
        var count = 0;
        $(mainmenuLinks).each(function() {
            var newLink = $(this);
            $(extramenu).prepend(newLink);
            count++;
        });
    }
}
